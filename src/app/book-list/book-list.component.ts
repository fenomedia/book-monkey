import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Book, Thumbnail} from '../shared/book';
import {BookStoreService} from '../shared/book-store.service';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'bm-book-list',
  templateUrl: './book-list.component.html',
  styles: []
})
export class BookListComponent implements OnInit {
  books$: Observable<Book[]>

  constructor(private bs: BookStoreService) {  }

  ngOnInit(){ // Wird aufgerufen wenn die Komponenten vollständig initialisiert ist
    //this.bs.getAll().subscribe(res => this.books = res);
    this.books$ = this.bs.getAll();
  }


}
