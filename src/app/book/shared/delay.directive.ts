import { Directive, OnInit , Input, TemplateRef, ViewContainerRef, HostBinding } from '@angular/core';

@Directive({
  selector: '[bmDelay]'
})
export class DelayDirective {
  @Input() bmDelay;

  constructor(
    private templateRef : TemplateRef<any>,
    private viewContainerRef : ViewContainerRef
  ) { }
  
  ngOnInit(){
    setTimeout(() => {
      this.viewContainerRef.createEmbeddedView(this.templateRef);
    }, this.bmDelay);
  }

}
