import {Thumbnail} from './thumbnail';
export {Thumbnail} from './thumbnail'; // Damit ist Thumbnail in Book-Importierenden Klassen auch gleich verfügbar

export class Book {
    constructor(
        public isbn: string,
        public title: string,
        public authors: string[],
        public published: Date,
        public subtitle?: string,
        public rating?: number,
        public thumbnails?: Thumbnail[],
        public description?: string
    ){}
}
