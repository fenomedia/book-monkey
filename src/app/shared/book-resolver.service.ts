import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Book } from './book';
import { BookStoreService } from './book-store.service';

@Injectable()
export class BookResolver implements Resolve<Book> {
  
  constructor(private bs : BookStoreService) { }
  
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):  Observable<Book> {
    return this.bs.getSingle(route.params['isbn']);
  }


}
