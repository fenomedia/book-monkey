export class ErrorMessage {
    constructor(
        public forControl:string,
        public forValidator:string,
        public text:string
    ){}
}

export const BookErrorMessages = [
    new ErrorMessage('title', 'required', 'Ein Buchtitel muss angegeben werden!'),
    new ErrorMessage('isbn', 'isbnFormat', 'Es muss eine ISBN mit 8-13 Zeichen eingegeben werden!'),
    new ErrorMessage('isbn', 'isbnExists', 'Die ISBN existiert schon!'),
    new ErrorMessage('published', 'required', 'Ein Buch muss mit einem Erscheinungsdatum angelegt werden!'),
    new ErrorMessage('authors', 'atLeastOneAuthor', 'Es muss mindestens ein Autor angegeben werden!')
];