import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { Book } from '../shared/book';
import { BookFactory } from '../shared/book-factory';
import { BookStoreService } from '../shared/book-store.service';
import { BookErrorMessages } from './book-form-error-messages';
import { Router, ActivatedRoute } from '@angular/router';
import { BookValidators } from '../shared/book.validators';
import { IsbnPipe } from '../shared/isbn.pipe';



@Component({
  selector: 'bm-book-form',
  templateUrl: './book-form.component.html',
  styles: []
})
export class BookFormComponent implements OnInit {

  // Zugriff auf die Formular-Komponente
  myForm: FormGroup;
  isUpdatingBook=false;
  authors:FormArray;
  thumbnails:FormArray;

  book: Book =  BookFactory.empty();
  errors: {[key: string]: string} = {};


  constructor(private bs: BookStoreService, 
              private fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute) { }

  submitForm(){
    // filter empty values
    this.myForm.value.authors.filter(author=>author);
    this.myForm.value.thumbnails.filter(thumbnail=>thumbnail.url);
    const book = BookFactory.fromObject(this.myForm.value);

    if(this.isUpdatingBook){
      this.bs.update(book).subscribe(res=>{
        this.router.navigate(['/books', this.book.isbn], {
          relativeTo: this.route
        })
      })
    }else{
      this.bs.create(book).subscribe(res => {
        this.book = BookFactory.empty();
        this.myForm.reset(BookFactory.empty());
        this.router.navigate(['/books'], {relativeTo:this.route})
      })
    }
  }

  ngOnInit() {
    const isbn = this.route.snapshot.params['isbn'];

    if(isbn){
      this.isUpdatingBook = true;
      this.bs.getSingle(isbn).subscribe(
        book => {
          this.book = book;
          this.initBook();
        }
      )
    }
    this.initBook();
  }

  initBook(){
    this.buildAuthorsArray();
    this.buildThumbnailsArray();

    this.myForm = this.fb.group({
        title: [this.book.title, Validators.required],
        subtitle: this.book.subtitle,
        isbn: [new IsbnPipe().transform(this.book.isbn, false), [
          Validators.required,
          BookValidators.isIsbnFormat
        ], this.isUpdatingBook ? null : BookValidators.isbnExists(this.bs)],
        description: this.book.description,
        authors: this.authors,
        thumbnails: this.thumbnails,
        published: this.book.published,
    });

    this.myForm.statusChanges.subscribe(() => this.updateErrorMessages())
  }

  buildAuthorsArray(){
    this.authors = this.fb.array(
      this.book.authors, BookValidators.atLeastOneAuthor
    )
  }

  buildThumbnailsArray(){
    this.thumbnails = this.fb.array(
      this.book.thumbnails.map(
        t => this.fb.group({
          url: this.fb.control(t.url),
          title: this.fb.control(t.title)
        })
      )
    )
  }

  addAuthorControl(){
    this.authors.push(this.fb.control(null));
  }

  addThumbnailsControl(){
    this.thumbnails.push(this.fb.group({url:null, title:null}));
  }

  updateErrorMessages(){
    this.errors = {};
    for(const message of BookErrorMessages){
      const control = this.myForm.get(message.forControl);
      if(control && // existiert
         control.dirty && // wurde bearbeitet 
         control.invalid && // nicht valide
         control.errors[message.forValidator] && // Wenn es eine Error-Message dazu gibt 
         !this.errors[message.forControl]) { // Wenn der Fehler nicht schon aufgenommen ist
            this.errors[message.forControl] = message.text;
         }  
    }
  }
}
