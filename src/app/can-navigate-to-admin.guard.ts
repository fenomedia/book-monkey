import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CanNavigateToAdminGuard implements CanActivate {
  private accessGranted = false;
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      if(!this.accessGranted){
        this.accessGranted = window.confirm('Fühlen Sie sich im Stande Bücher zu administrieren ;) ?');
      }
      return this.accessGranted;
  }
}
